Dev Degany - Java Masterclass
=============================
Badass content, for badass developers:<br>

*  DDj1. The '+' Operator and String: a Dreadful Wedding
*  DDj2. Formal Arguments: Systematic Copy
*  DDj3. Checked vs Unchecked Exceptions: the Unending War

Test Environment
================
```
Machine:
  Arch: amd64
  Proc: 4
  OS: Linux 4.15.0-47-generic
Java:
  Version: 1.8.0_191
  Runtime: 1.8.0_191-8u191-b12-2ubuntu0.16.04.1-b12
  TotalMem: 124MB
  MaxMem: 1840MB
```

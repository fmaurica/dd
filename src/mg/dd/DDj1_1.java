package mg.dd;

public class DDj1_1 {
  public static void main(final String[] args) {
    long t0 = System.currentTimeMillis();
    long um0 = Utils.usedMemoryMB();

    String/* IMMUTABLE */ s = "";
    for (int i = 0; i < 50_000/* '_' in numbers */; i++)
      s = s + i + " ";
    System.out.println(s);

    System.out.println(System.currentTimeMillis() - t0); // 5227
    System.out.println(Utils.usedMemoryMB() - um0); // 208
  }
}
package mg.dd;

public class DDj1_2 {
  public static void main(final String[] args) {
    long t0 = System.currentTimeMillis();
    long um0 = Utils.usedMemoryMB();

    String s = "";
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < 50_000; i++)
      sb.append/* Read its implementation */(Integer.toString(i))
        .append(" ");
    s = sb.toString();
    System.out.println(s);

    System.out.println(System.currentTimeMillis() - t0); // 42
    System.out.println(Utils.usedMemoryMB() - um0); // 4
    /* StringBuilder is faster. HOWEVER, it is NOT thread-safe! */
  }
}
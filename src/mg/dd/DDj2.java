package mg.dd;

class Man {
  private int age;

  public Man(int age) {
    this.age = age;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }
}

public class DDj2 {
  private static void incrAge1(
      Man man/* Formal argument: **COPY** of the actual argument */) {
    Man newman = new Man(man.getAge() + 1);
    man = newman;
  }

  private static void incrAge2(Man boy) {
    boy.setAge(boy.getAge() + 1);
  }

  public static void main(String[] args) {
    Man lou = new Man(25);
    incrAge1(lou/* Actual argument */);
    System.out.println(lou.getAge() == 26); // false
    incrAge2(lou);
    System.out.println(lou.getAge() == 26); // true
  }
}
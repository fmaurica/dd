package mg.dd;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DDj3 {
  public static void main(String[] args) {
    try {
      Files.readAllBytes(Paths.get(""));
    } catch (IOException/*(statically) checked exception*/ e) {}

    assert 3/0 == 3/0; // UNchecked exception
    /* Pedantic observation:
     * Using mathematical logic where partial functions
     * are modeled by under-specified total functions,
     * the above assertion would be true
     * as reflexivity of equality is preserved.
     * However, that conclusion cannot be obtained at runtime:
     * this is known as the undefinedness problem,
     * see Y. Cheon's PhD, section 3.2 */
  }
}
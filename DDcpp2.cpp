#include <iostream>

class Man {
  int age;
  public:
    explicit Man(int age);
    int getAge() const;
};

Man::Man(int age) {
  this->age = age;
}

int Man::getAge() const {
  return age;
}

void incrAge(Man &man) {
  man = *new Man(man.getAge() + 1);
  /* WARNING: that `new` causes memory leak,
   * this is just for comparing with Java.
   * NEVER do that! */
}

int main() {
  Man lou(25);
  incrAge(lou);
  std::cout << lou.getAge(); // 26
  return 0;
}
